import { Injectable, NestMiddleware, Logger, Req } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware{
    use(req: any, res: any, next: () => void){
        Logger.log('Hero API Request: ' + req.baseUrl + req.url);
        next();
    }
}