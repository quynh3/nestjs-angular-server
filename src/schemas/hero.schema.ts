import * as mg from 'mongoose';
export const HeroSchema = new mg.Schema({
    name: {
        type: String,
        required: [true, 'Please enter name hero'],
    },
    universe: {
        type: String,
        required: [true, 'Please enter universe hero'],
    },
});