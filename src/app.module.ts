import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { HeroModule } from './hero/hero.module';
import { LoggerMiddleware } from './shared/middlewares/logger.middleware';


@Module({
  imports: [MongooseModule.forRoot(process.env.MONGODB_CLOUD,{
    useNewUrlParser: true,
  }), HeroModule,
],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer){
    consumer.apply(LoggerMiddleware).forRoutes('hero');
  }
}
