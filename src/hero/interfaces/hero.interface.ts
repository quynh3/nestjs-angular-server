export interface objHero{
    id?: string; // '?' means: can be null
    name: string;
    universe: string;
}