import { Injectable, Logger, HttpException, HttpStatus } from '@nestjs/common';
import {objHero} from './interfaces/hero.interface';
import {Model} from 'mongoose';
import {InjectModel} from '@nestjs/mongoose';

@Injectable()
export class HeroService {

    constructor(
        @InjectModel('Hero') private readonly heroModel: Model<objHero>,
        ){}

    async getHero(): Promise<objHero[]> {
        try{
            return await this.heroModel.find().exec();
        } catch (err){
            throw new HttpException('Hero not found', HttpStatus.NOT_FOUND);
        } 
    }

    async getHeroes(id: string): Promise<objHero>{
        return await this.heroModel.findById(id).exec();
    }

    async createHero(hero: objHero): Promise<objHero>{
        try {
            const newHero = await new this.heroModel(hero);
            return newHero.save();
        } catch (err){
            throw new HttpException(err.message, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    async updateHero(id: string, updateHeroesDto): Promise<objHero>{
        return await this.heroModel.findByIdAndUpdate(id, updateHeroesDto, {
            new: true
        });
    }

    async deleteHero(id: string): Promise<any>{
        return await this.heroModel.findByIdAndRemove(id);
    }
}
