import { Module } from '@nestjs/common';
import { HeroController } from './hero.controller';
import { HeroService } from './hero.service';
import { MongooseModule, Schema } from '@nestjs/mongoose';
import { HeroSchema } from 'src/schemas/hero.schema';

@Module({
    imports: [MongooseModule.forFeature([{name: 'Hero', schema: HeroSchema}]),
],
    controllers: [HeroController],
    providers: [HeroService],
}) 
export class HeroModule {}
