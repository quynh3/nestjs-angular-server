import { ApiProperty } from "@nestjs/swagger";

export class CreateHeroDto{
    @ApiProperty()
    readonly name: string;
    @ApiProperty()
    readonly universe: string;
}