import { Controller, Get, Post, Body, Param, Put, Delete } from '@nestjs/common';
import {HeroService} from './hero.service';
import { CreateHeroDto } from './dto/create-hero.dto';
import { objHero } from './interfaces/hero.interface';
import {ApiTags, ApiParam} from '@nestjs/swagger';

@ApiTags('hero')
@Controller('hero')
export class HeroController {
    constructor(private heroService: HeroService){}
    @Get() //localhost:3000/hero
    getHero(): Promise<objHero[]>{
        return this.heroService.getHero();
    }

    @ApiParam({name: 'id'})
    @Get(':id')
    getHeroes(@Param('id') id): Promise<objHero>{
        return this.heroService.getHeroes(id);
    }

    @Post()
    createHeroes(@Body() createHeroDto: CreateHeroDto): Promise<objHero>{
        return this.heroService.createHero(createHeroDto);
    }

    @ApiParam({name: 'id'})
    @Put(':id')
    updateHeroes(@Param('id') id, @Body() updateHeroesDto: CreateHeroDto): Promise<objHero> {
        return this.heroService.updateHero(id, updateHeroesDto);
    }

    @ApiParam({name: 'id'})
    @Delete(':id')
    deleteHeroes(@Param('id') id): Promise<any>{
        return this.heroService.deleteHero(id);
    }
}
